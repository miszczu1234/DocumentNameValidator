package pl.com.tt.test.documentvalidator;

import org.junit.Before;
import org.junit.Test;
import pl.com.tt.documentvalidator.DocumentNameValidator;
import static org.junit.Assert.assertEquals;

public class TestDocumentNameValidator {

    private DocumentNameValidator documentValidator;

    @Before
    public void beforeTest() {
        documentValidator = new DocumentNameValidator();
    }

    @Test
    public void testDocumentNameValidatorSuccess() {
        String[] data = {"aaaa_Sheet_000.tiff", "b_Sheet_000_111.png"};

        assertEquals(true, documentValidator.isValid(data[0]));
        assertEquals("aaaa", documentValidator.getFileName());
        assertEquals("000", documentValidator.getNumber());
        assertEquals("tiff", documentValidator.getExtension());

        assertEquals(true, documentValidator.isValid(data[1]));
        assertEquals("b", documentValidator.getFileName());
        assertEquals("000_111", documentValidator.getNumber());
        assertEquals("png", documentValidator.getExtension());
    }

    @Test
    public void testDocumentNameValidatorFailure() {
        String data[] = {"Sheet_000.tiff", "aaa_Sheet_0png", "aaa_Sheet_png"};
        for (String entry : data) {
            assertEquals(false, documentValidator.isValid(entry));
        }
    }
}
