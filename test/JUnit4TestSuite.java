
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import pl.com.tt.test.documentvalidator.TestDocumentNameValidator;

@RunWith(Suite.class)
@Suite.SuiteClasses(value={TestDocumentNameValidator.class})
public class JUnit4TestSuite {
}
