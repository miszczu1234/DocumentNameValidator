package pl.com.tt.documentvalidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternDecorator {

    private String patternStr;
    private Matcher lastMatcher;

    public PatternDecorator(String patternStr) {
        this.patternStr = patternStr;
    }

    public String getPatternStr() {
        return new String(patternStr);
    }

    public boolean isMatch(String data) {
        Pattern pattern = Pattern.compile(patternStr);
        lastMatcher = pattern.matcher(data);
        return lastMatcher.matches();
    }

    public Matcher getMatcher() {
        return lastMatcher;
    }
}
