package pl.com.tt.documentvalidator;

public class DocumentNameValidator {

    private static final String DOCUMENT_NAME_PATTERN = "(.*?)_Sheet_(.*)\\.(.*)";
    private PatternDecorator parser;
    private boolean valid;

    public DocumentNameValidator() {
        valid = false;
        parser = new PatternDecorator(DOCUMENT_NAME_PATTERN);
    }

    public boolean isValid(String documentName) {
        valid = parser.isMatch(documentName);
        return valid;
    }

    ;
    
    private String getResultGroup(int group) {
        if (valid) {
            return parser.getMatcher().group(group);
        }

        return "";
    }

    public String getFileName() {
        return getResultGroup(1);
    }

    public String getNumber() {
        return getResultGroup(2);
    }

    public String getExtension() {
        return getResultGroup(3);
    }
}
